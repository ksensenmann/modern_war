// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Modern_WarGameMode.generated.h"

UCLASS(minimalapi)
class AModern_WarGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AModern_WarGameMode();
};



