// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Modern_WarGameMode.h"
#include "Modern_WarCharacter.h"
#include "UObject/ConstructorHelpers.h"

AModern_WarGameMode::AModern_WarGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
